# Zsh config file

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

source ~/.config/zsh/.zprofile

# Include Cargo bin folder to PATH.
export PATH="~/.cargo/bin:${PATH}"

# Include Emacs bin folder to PATH.
export PATH="~/.config/emacs/bin:${PATH}"

# Include personal scripts from dotfiles directory.
export PATH="~/.dotfiles/bin:${PATH}"

# Include personal scripts.
export PATH="~/.local/bin:${PATH}"

export PYTHONPATH=${PYTHONPATH}:~/School/projPrax/symboliclib/symboliclib
#export PYTHONPATH=${PYTHONPATH}:~/School/projPrax/z3/build/python

# Path to your oh-my-zsh installation.
ZSH="/home/adda/.config/zsh/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="robbyrussell"
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.

# Plugins to test: yarn rake cloudfoundry npm node
plugins=(
    git
    zsh-autosuggestions
    fast-syntax-highlighting
    #z # Using zoxide instead.
    sudo # Hitting `ESC` twice puts sudo in front of the current command, or the last one if the command line is empty.
    vscode # Makes interaction between the command line and the code editor easier.
    last-working-dir
    dotenv
    web-search
    history
    extract
    #auto-notify
    #thefuck
)

# command-not-found plugin.
# To enable command-not-found hook for your shell
# add any of the following to your .bashrc or .zshrc file
# depending on the shell you use:
# for bash:
#source /usr/share/doc/find-the-command/ftc.bash
# for zsh:
#source /usr/share/doc/find-the-command/ftc.zsh
# You can also append 'su' option to use su instead of sudo
# for root access, 'noprompt' to disable installation
# prompt at all, or 'quiet' to decrease verbosity, e.g.:
#source /usr/share/doc/find-the-command/ftc.zsh noprompt quiet

# It is necessary to create pacman files database, run
# pacman -Fy
# You may also want to enable timer for regular
# pacman files database update:
# systemctl enable pacman-files.timer

# dotenv plugin: in ~/.zshrc, before Oh My Zsh is sourced:
ZSH_DOTENV_FILE=.dotenv

source $ZSH/oh-my-zsh.sh

# Thefuck configuration.
# TODO: Enable instant mode with --enable-experimental-instant-mode.
eval "$(thefuck --alias  f)"

# Customize oh-my-zsh plugins.
# unalias g

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.

# Environmental variables.
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export MANROFFOPT="-c"

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='nvim'
# else
#   export EDITOR='nvim'
# fi
#export EDITOR='nvim'
#export EDITOR="emacsclient -c -a 'emacs'" # $EDITOR use Emacs in terminal
#export VISUAL="emacsclient -c -a 'emacs'" # $VISUAL use Emacs in GUI mode
export EDITOR="helix" # $EDITOR use Helix in terminal
export VISUAL="helix" # $VISUAL use Helix in GUI mode

export RANGER_LOAD_DEFAULT_RC=FALSE
export DOTFILES="~/.dotfiles"
# Created by `userpath` on 2020-03-16 15:46:44
export PATH="$PATH:~/.local/bin"

# Enable colors and change prompt:
autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "


# zsh-auto-notify options.
#export AUTO_NOTIFY_THRESHOLD=10
#export AUTO_NOTIFY_EXPIRE_TIME=10000
#AUTO_NOTIFY_IGNORE+=("lg")
#export AUTO_NOTIFY_EXPIRE_TIME=5

#DEFAULT_USER=your_user_name


# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zshhistory
setopt appendhistory

setopt auto_pushd

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots) # show hidden files in TAB completion.
bindkey -M menuselect '^M' .accept-line

# Initialize zoxide.
eval "$(zoxide init zsh)"

# vi mode TODO what to keep and what to delete
set -o vi
bindkey -v

function print_terminal_line_sparklines {
    seq 1 $(tput cols) | sort -R | sparklines | lolcat
}

function print_terminal_line_sparklines_newline {
    print_terminal_line_sparklines
    echo
}

function clear-screen {
    echoti clear
    print_terminal_line_sparklines_newline
    echo
    zle redisplay
}
zle -N clear-screen

xmodmap -e 'keycode 105 = Menu'

zstyle ':completion:*:descriptions' format '%B%d%b'
zstyle ':completion:*:messages' format '%d'
zstyle ':completion:*:warnings' format 'No matches for: %d'
zstyle ':completion:*' group-name ''

# Autocomplete Git branches names without lag
__git_files () {
    _wanted files expl 'local files' _files
}
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
source ~/.config/zsh/.p10k.zsh
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

# Load ; should be last.
source /usr/share/autojump/autojump.zsh 2>/dev/null
#source ~/powerlevel10k/powerlevel10k.zsh-theme

# ZSH autocompletion in git-extras.
source /usr/share/doc/git-extras/git-extras-completion.zsh

# Override universal settings with local settings
if [ -f ~/.config/zsh/.zshrc_local ]; then
    source ~/.config/zsh/.zshrc_local
fi

# Auto-run when new terminal prompt is started.
#neofetch
#colorscript random # Show random color script (DT's color scripts).
print_terminal_line_sparklines_newline

# Code dumps.
ulimit -c unlimited

[ -f "/home/adda/.ghcup/env" ] && source "/home/adda/.ghcup/env" # ghcup-env

if type rg &> /dev/null; then
  export FZF_DEFAULT_COMMAND='rg --files'
  export FZF_DEFAULT_OPTS='-m'
fi

# Atuin configuration.
export ATUIN_NOBIND="true"
eval "$(atuin init zsh)"
bindkey '^r' atuin-search
# bind to the up key, which depends on terminal mode
bindkey '^[[A' atuin-up-search
bindkey '^[OA' atuin-up-search
source "$HOME/.atuin/bin/env"

# End of file.

