# File name: custom_aliases.zsh
#
# oh-my-zsh custom aliases config file

# Sudo aliases.
#alias systemctl='sudo systemctl'

alias xbindkeys="xbindkeys -f ${XDG_CONFIG_HOME}/xbindkeys/config"

alias cdr="source ranger"

function update {
  rustup update
  paru
  flatpak update
  cargo install-update -a
  nix profile upgrade --all
  uv tool upgrade --all
}

function clean-system {
  cargo cache -a
  paru -Scccd
  flatpak uninstall --unused
  ccache -C
  trash-empty
  #nix-collect-garbage
  nh clean all
  #rm -rf ~/.cache/vscode-cpptools/*
  sudo flatpak repair
  sudo rm -rf /var/lib/systemd/coredump/*
  sudo trash-empty
}

# Processes.
alias ps="ps auxf" # show all processes, display user-oriented format, show processes that aren't attached to ttys, use full-format listing
alias pse="\ps -e --forest" # list processes as a tree
alias psg="\ps aux | grep -v grep | grep -i -e VSZ -e" # search for a running process. example: psg firefox
alias psr="\ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%cpu | head" # list top 10 cpu-intensive processes
alias pst="\ps -eo pid,comm,lstart,etimes,time,args"

## get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

## get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

# Tar commands
alias tgz='tar -xvfz'
alias tarc="tar -cvzf"
alias tarx="tar -xvf"

# Shutdown or reboot.
alias shutdown="sudo shutdown now"
alias off="shutdown -h now"
alias reboot="sudo reboot"

# libre office convert docx and pptx to pdf
alias lowpdf="lowriter --convert-to pdf"

# fzf wrappers
alias fzfcd='fzfcd() { cd "$(find -type d 2>/dev/null | fzf)" ;}; fzfcd'
alias fzfed='fzfed() { $EDITOR "$(find -type f 2>/dev/null | fzf)" ;}; fzfed'

# Find.
#alias fd="find -name"
alias fd="fd"
alias fcd='find . -type d -name' # find directory starting from current directory
alias fcf='find . -type f -name' # find file starting from current directory
alias findbig='fd --type f --exec du -Sh {} | sort -rh | head'
alias findbig='du -Sh . | sort -rh | head'

alias deleteempty="find . -type d -empty -delete"
alias listext="fd --type f | awk -F. '!a[$NF]++{print $NF}'"
alias tofolder='for t in *.*; do mkdir -p "${t%.*}"; mv "$t" "${t%.*}"; done'

# TODO: I have nice fish function for moving folders without overwriting…
#function rmv --description 'Move files using rsync'
#for file in $argv[1..-2]
#printf "Moving \033[0;31m $file \033[0m \n"; rsync -ahhh --stats --inplace --info=progress2 --ignore-existing --remove-sent-files $file $argv[-1]
#find $file -depth -type d -empty -delete
#end
#end

# General aliases
# alias o="re.sonny.Junction"
alias c="clear -x && print_terminal_line_sparklines" # Clear while keeping the buffer.
alias cf="clear && print_terminal_line_sparklines" # Clear the buffer, too.
alias srczsh="exec zsh"
alias ra="ranger"
alias cpr="cp -r"
alias v="nvim"
alias n="nvim"
alias ne="nemo 2>/dev/null &"  # Open Nemo in home directory.
alias nec="nemo . 2>/dev/null &"  # Open Nemo in the current directory.
alias nv='nvim'

# Config files
## Copy config files
alias cpconf="
  cpgitconfig ;
  cpzshrc ;
  cpalias ;
  cpnvim ;
  cpranger ;
  cptigrc ;
  srczsh && c
"
## Open config files
alias dotf="cd ~/.dotfiles/"
alias dotfiles="cd ~/.dotfiles/"
alias aliases="${=EDITOR} $HOME/.oh-my-zsh/custom/custom_aliases.zsh"

# Apt commands.
alias sai="sudo apt install -y -V"
alias dai="doas apt install -y -V"
alias sau="sudo apt update && sudo apt upgrade -y"
alias dau="doas apt update -V && doas apt upgrade -y -V"
alias sap="sudo apt purge -y -V"
alias dap="doas apt purge -y -V"
alias sar="sudo apt autoremove -y -V"
alias dar="doas apt autoremove -y -V"
alias rem='sudo apt autoremove --purge -V'

# Pacman commands
alias pmu='sudo pacman -Syyu'
alias ple="pacman --query --explicit | awk '{ print \$1 }' " # Pacman list explicit.
alias pla="pacman --query --quiet " # Pacman list all installed packages names without versions.

# Snap commands
alias snapr="snap refresh"

# Git aliases.
## Git command.
alias git='git-branchless wrap --'
# function g {
#   git-branchless wrap --
# }
alias g='git-status-or-git-command'

## Git TUI managers.
alias t='tig status'
alias tt='tig'

alias gui='gitui'

## Git shortcuts.
alias gi="g i"
alias gb='g cob'
alias gcob="g cob"
alias gc="g c"
alias gcm="g cm"
alias gcom="g cm"
alias gcs="g cs"
alias gap="g ap"
alias gaa="g aa"
alias gaaa="g aaa"
alias glga="g lga"
alias ga="g a"
alias gp="g p"
alias gpa="g pa"
alias gpfa="g pfa"
alias gpl="g pl"
alias gd='g d'
alias gars="g ars"
alias gr="g r"
alias gpo="git pull origin"
alias gpom="git pull origin master"
alias gpp="gp personal"
alias gppm="gp personal master"
alias gs='g s'

#TODO To review.
#alias gbd="git branch -D"
#alias gca="git commit -a -m"
#alias gm="git merge --no-ff"
#alias gpt="git push --tags"
#alias grh="git reset --hard"
#alias gb="git branch"
#alias gba="git branch -a"
#alias gcp="git cherry-pick"
#alias gl="git log --pretty='format:%Cgreen%h%Creset %an - %s' --graph"
#alias gcd='cd "`git rev-parse --show-toplevel`"'
#alias gr='git rm -r '
#alias gc-='git checkout - '

function acp() {
  gaaa && gcs && gp
}

# Forbid pushing to selected remote repository.
function git_remote_no_push() {
    if [ -n "$1" ]
    then
        git remote set-url --push "$1" no_push;
    else
      echo "error: specify remote to disable pushing to";
      return 1;
    fi
}
alias grnp="git_remote_no_push"

# Update fonts
alias font_update="fc-cache -f -v"

# GPG aliases
alias gpgl="gpg --list-secret-keys --keyid-format LONG"
alias gpgg="gpg --full-generate-key"
alias gpge="gpg --armor --export $0"

# SSH key aliases
# Lists the files in your .ssh directory, if they exist.
alias sshl="ls -al $HOME/.ssh"
# Generating public/private rsa key pair.
# ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
function sshg {
    ssh-keygen -t rsa -b 4096 -C "$0"
}

# Print my public IP
alias myip='curl ipinfo.io/ip'
#alias myip="curl --silent https://ipecho.net/plain; echo"

# Print website IP address
alias pingo="ping -c 1"

# Clone GitHub repository with ssh
function gcl {
    git clone ssh://git@github.com/$1/$2.git;
}

# Rename Terminal Window - Usage: wn 'window name'
function wn {
    printf "\e]2;$1\a"
}

function cdls {
    builtin cd "$@"; l;
}

# Rename Terminal Tab - Usage: tn 'tab name'
function tn {
    printf "\e]1;$1\a"
}

# Make directory and cd into it – usage: mkcd 'dir_name'
function mkcd {
    mkdir -p -- "$1" && cd -P -- "$1"
}
alias mdcd="mkcd"

### ARCHIVE EXTRACTION ###
# usage: ex <archive_file>
function ex {
  if [ -f "$1" ] ; then
    case "$1" in
      *.tar.bz2)   tar xjf "$1"    ;;
      *.tar.gz)    tar xzf "$1"    ;;
      *.bz2)       bunzip2 "$1"    ;;
      *.rar)       unrar x "$1"    ;;
      *.gz)        gunzip "$1"     ;;
      *.tar)       tar xf "$1"     ;;
      *.tbz2)      tar xjf "$1"    ;;
      *.tgz)       tar xzf "$1"    ;;
      *.zip)       unzip "$1"      ;;
      *.Z)         uncompress "$1" ;;
      *.7z)        7z x "$1"       ;;
      *.deb)       ar x "$1"       ;;
      *.tar.xz)    tar xf "$1"     ;;
      *.tar.zst)   unzstd "$1"     ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

alias aliasa='alias | less' # Show all aliases.

# Colorize grep output (good for log files).
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# Play audio files in current dir by type.
alias playwav='deadbeef *.wav'
alias playogg='deadbeef *.ogg'
alias playmp3='deadbeef *.mp3'

# Play video files in current dir by type.
alias playavi='vlc *.avi'
alias playmov='vlc *.mov'
alias playmp4='vlc *.mp4'


#alias rm='trash'
# adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias lynx='lynx -cfg=$HOME/.lynx/lynx.cfg -lss=$HOME/.lynx/lynx.lss -vikeys'
alias vifm='./.config/vifm/scripts/vifmrun'


# Merge Xresources
alias merge='xrdb -merge $HOME/.Xresources'

# get error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# gpg encryption
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# youtube-dl
alias ytdl='youtube-dl'
alias yta-aac="youtube-dl --extract-audio --audio-format aac "
alias yta-best="youtube-dl --extract-audio --audio-format best "
alias yta-flac="youtube-dl --extract-audio --audio-format flac "
alias yta-m4a="youtube-dl --extract-audio --audio-format m4a "
alias yta-mp3="youtube-dl --extract-audio --audio-format mp3 "
alias yta-opus="youtube-dl --extract-audio --audio-format opus "
alias yta-vorbis="youtube-dl --extract-audio --audio-format vorbis "
alias yta-wav="youtube-dl --extract-audio --audio-format wav "
alias ytv-best="youtube-dl -f bestvideo+bestaudio "

# switch between shells
alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
alias tofish="sudo chsh $USER -s /bin/fish && echo 'Now log out.'"

# bare git repo alias for dotfiles
alias bareconfig="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"

# Python
alias py='python'

alias sgrep='grep -R -n -H -C 5 --exclude-dir={.git,.svn,CVS} '

alias ta='tail -f'

# Command line head / tail shortcuts
alias -g H='| head'
alias -g T='| tail'
alias -g G='| grep'
alias -g L="| less"
alias -g M="| most"
alias -g LL="2>&1 | less"
alias -g CA="2>&1 | cat -A"
alias -g NE="2> /dev/null"
alias -g NUL="> /dev/null 2>&1"
alias -g P="2>&1| pygmentize -l pytb"

alias dud='du -d 1 -h'
alias dufi='du -sh *'

alias h='history'
alias dirs='dirs -v | head -10'
alias hgrep="fc -El 0 | grep"
#alias p='ps -f'
alias sortnr='sort -n -r'
alias unexport='unset'

alias gds-start='sudo systemctl start openvpn-client@gds'
alias gds-stop='sudo systemctl stop openvpn-client@gds'

alias stv_mapstart="xbindkeys"
alias stv_mapstop="killall xbindkeys"
alias stv_smapi_run='alacritty --command ~/.local/share/Steam/steamapps/common/Stardew\ Valley/StardewModdingAPI --mods-path ~/.config/Stardrop/Data/Selected\ Mods &'
alias stv_stardrop_run='alacritty --command ~/documents/modding/StardewValley/Stardrop-repo/Stardrop/bin/Release/Stardrop &'

alias run_rimworld='alacritty --command ~/.steam/steam/steamapps/common/RimWorld/RimWorldLinux &'
alias run_rimsort='alacritty --command python ~/documents/modding/RimWorld/RimSort-repo/RimSort.py &'

# Cheat.sh cheat sheet.
alias ccht='curl cht.sh/'

# Emacs.
alias doom="~/.config/emacs/bin/doom"
alias ec="emacsclient -c -a 'emacs'"

alias isvenv="echo '${VIRTUAL_ENV}'"

# Battery state
alias battery="acpi -bi"

# End of file.
